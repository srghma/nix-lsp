{ nixpkgs ? import ./nix/nixpkgs }:

let
  overlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);

  pkgs = import nixpkgs { overlays = [ overlay ]; };
in

with pkgs;

stdenv.mkDerivation {
  name = "rust-env";

  buildInputs = [
    latest.rustChannels.nightly.rust

    pkgconfig
    openssl
  ];

  RUST_BACKTRACE = 1;
}
