{ rust, buildRustPackage }:

buildRustPackage {
  name = "nix-lsp";
  src = builtins.filterSource (path: _type: path != (toString ./target)) ./.;
  cargoSha256 = "06qfnk0sq4a8f7vwmgwwn9ifkzngp53r4q61a2739rbgc17z0d1b";

  # See https://github.com/NixOS/nixpkgs/issues/25863#issuecomment-302633494
  RUSTFLAGS="-L ${rust}/lib/rustlib/x86_64-unknown-linux-gnu/lib/";
}
