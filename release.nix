{
  nixpkgs ? import ./nix/nixpkgs,
  pkgs ?
    let
      mozilla-overlay = import(builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
    in
      import nixpkgs { overlays = [ mozilla-overlay ]; }
}:

let
  rustChannel = pkgs.latest.rustChannels.nightly;

  buildRustPackage = pkgs.rustPlatform.buildRustPackage.override {
    rust = rustChannel;
  };
in
  import ./default.nix {
    inherit buildRustPackage;
    rust = rustChannel.rust;
  }
